<?php 

if(isset($_SESSION['login_user']))
{
    echo  '<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Serenity Star</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="bookings.php">My bookings</a></li>
      <li><a href="feedback.php">Feedback</a></li>
      <li><a href="index.php">Villas</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>';
     
}else{
    
    echo  '<nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">Serenity Star</a>
        </div>
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Home</a></li>
          <li><a href="ContactUs.php">Contact us </a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="SignUp.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
          <li><a href="Login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        </ul>
      </div>
    </nav>';
}

    
    ?>