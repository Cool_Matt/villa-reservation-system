<?php 
session_start();
include("Config.php");
if(!isset($_SESSION["login_user"])){
	header("location: index.php?msg=no backdoor!");
    exit();
}

$message ="";
if(isset($_POST))
{
    $customer_id = $_POST['customer_id'];
    $villa_id = $_POST["villa_id"];
    $checkin = $_POST["checkin"];
    $checkout = $_POST["checkout"];
    $no_of_days = $_POST["no_of_days"];
    $total_guest = $_POST["total_guest"];
        
    $getUserDetails = "select * from customers where id = '$customer_id'";
    $getUserDetailsresult = mysqli_query($db,$getUserDetails);
    $getUserDetailsrow = mysqli_fetch_array($getUserDetailsresult,MYSQLI_ASSOC);

      $first_name = $getUserDetailsrow['first_name'];
      $last_name = $getUserDetailsrow['last_name'];
      $userid = $getUserDetailsrow['id'];

    $sql = "select * from villas where id = '$villa_id'";
    $result = mysqli_query($db, $sql);
    
    while($row = mysqli_fetch_array($result))
    {
        //$id = $row['id'];
        $villa_name = $row['name'];
        $address_line1 = $row['address_line1'];
        $address_line2 = $row['address_line2'];
        $postcode = $row['postcode'];
        $city = $row['city'];
        $daily_cost = $row['daily_cost'];
        $short_description = $row['short_description'];
        $main_description = $row['main_description'];
        $img_link = $row['img_link'];
    }
    
    $totalcost = $daily_cost * $no_of_days;
}





include 'header.php'; 
?>


<div class="villa_banner" style="background-image:url('<?php echo $img_link;?>')">             
  </div>
<?php include 'nav.php'; ?>

<div id="content_area">
  <div class="col-md-12" style="margin-top:10px; margin-bottom:10px;">
        <div class="col-md-6">
            <div class="panel panel-primary">
      <div class="panel-heading"><h1> <?php echo $villa_name ; ?> </h1></div>
      <div class="panel-body"><?php echo $address_line1 . ", ". $address_line2 . ", " . $postcode; ?>
        <p>
          <?php echo $short_description ;?>
        </p>
        <p>
            <?php echo $main_description ; ?>
        </p>
        </div></div>
    </div> 
            
            
        
        <div class="col-md-6" style="border:1px solid #ccc; background:#ffdbab; min-height:400px;  padding:10px;">
            
            <h2> Confirm your reservation </h2>
            
           <?php echo $message; ?>
            <form method="post" action="process_booking.php">
                <div class="form-group">
                  <label>  Customer first name : </label>
                    <input  type="text" readonly class="form-control" name="booking_first_name" value="<?php echo $first_name ;?>"/>
                </div>
                
                <div class="form-group">
                  <label> Customer last name : </label>
                    <input  type="text" readonly  class="form-control" name="booking_last_name" value="<?php echo $last_name ;?>"/>
                </div>
                
                <div class="form-group">
                   <label> Villa Name : </label>
                    <input  type="text" readonly  class="form-control" name="booking_villa_name" value="<?php echo $villa_name ;?>"/>
                </div>
                
              <div class="form-group">
                    <label> Check in date : </label>
                    <input  type="text" readonly  class="form-control" name="booking_checkin" value="<?php echo $checkin ;?>"/>
                </div>        
              <div class="form-group">
                    <label> Check out date : </label>
                    <input  type="text" readonly  class="form-control" name="booking_checkout" value="<?php echo $checkout ;?>"/>
                </div>
                <div class="form-group">
                    <label> Length of stay(days) : </label>
                    <input  type="text" readonly  class="form-control" name="booking_days" value="<?php echo $no_of_days ;?>"/>
                </div>
                  <div class="form-group">
                    <label> Total guests : </label>
                    <input  type="text" readonly  class="form-control" name="booking_guests" value="<?php echo $total_guest ;?>"/>
                </div>
                  <div class="form-group">
                    <label> Total cost(GBP) : </label>
                    <input  type="text" readonly  class="form-control" name="booking_total_cost" value="<?php echo $totalcost ;?>"/>
                </div>
                
                <div class="form-group">
                    <label> Notes </label>
                    <textarea name="booking_notes" class="form-control" rows="3"> </textarea>
                </div>
                       <input type="hidden" name="booking_customer_id" readonly  value="<?php echo $userid ;?>"/>
                  <input  type="hidden" name="booking_villa_id" readonly  value="<?php echo $villa_id ;?>"/>
                
                <input type="submit" name="submit" class="btn btn-primary"/>
                </div>
            </form>
            
        </div>
 </div>
 