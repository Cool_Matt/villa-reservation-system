<?php 
session_start();
include("Config.php");
if(!isset($_SESSION["login_user"])){
	header("location: index.php?msg=no backdoor!");
    exit();
}

$message ="";
if(isset($_GET['id']))
{
    $id = $_GET['id'];
    
    $customer_id = $_SESSION["user_id"];
    
    $sql = "select * from villas where id = '$id'";
    $result = mysqli_query($db, $sql);
    
    while($row = mysqli_fetch_array($result))
    {
        $return_id = $row['id'];
        $return_villa_name = $row['name'];
        $return_address_line1 = $row['address_line1'];
        $return_address_line2 = $row['address_line2'];
        $return_postcode = $row['postcode'];
        $return_city = $row['city'];
        $return_daily_cost = $row['daily_cost'];
        $return_short_description = $row['short_description'];
        $return_main_description = $row['main_description'];
        $return_img_link = $row['img_link'];
    }
}

include 'header.php'; 
?>

<script>
 $( function() {
     
     
    $( "#check-in" ).datepicker({ 
        dateFormat: 'yy-mm-dd' ,
        minDate: 0 
    }).val();
     
    $( "#check-out" ).datepicker({ 
        dateFormat: 'yy-mm-dd' ,
        minDate:0
        
    }).val();
     
    $("#check_availability").click(function() {
      //alerting the value inside the textbox
      var rawdate1 = $("#check-in").datepicker("getDate");
      var check_in_date = $.datepicker.formatDate("yy-mm-dd", rawdate1);
       
      var rawdate2 = $("#check-out").datepicker("getDate");
      var check_out_date = $.datepicker.formatDate("yy-mm-dd", rawdate2);  
    
        $("#hidden_checkin").val(check_in_date);
        $("#hidden_checkout").val(check_out_date);;
        
        var diff =  Math.floor(( Date.parse(check_out_date) - Date.parse(check_in_date) ) / 86400000); 
        
        var dayText = ""
        if(diff == 1){
            dayText ="day";
        }else{
            dayText= "days"
        }
        
      $("#amount_of_days").append(diff + " " + dayText);
        $("#no_of_days").val(diff);
      
        
        var totalguests = parseInt($("#ddl_no_of_adults").val()) + parseInt($("#ddl_no_of_children").val());
        
        
        
        $("#total_guest").val(totalguests);
        
        
    });
     
  } );
</script>


<div class="villa_banner" style="background-image:url('<?php echo $return_img_link;?>')">             
  </div>
<?php include 'nav.php'; ?>

<div id="content_area">
  <div class="col-md-12" style="margin-top:10px; margin-bottom:10px;">
        <div class="col-md-6">
            <div class="panel panel-primary">
      <div class="panel-heading"><h1> <?php echo $return_villa_name ; ?> </h1></div>
      <div class="panel-body"><?php echo $return_address_line1 . ", ". $return_address_line2 . ", " . $return_postcode; ?>
        <p>
          <?php echo $return_short_description ;?>
        </p>
        <p>
            <?php echo $return_main_description ; ?>
        </p>
        </div></div>
    </div> 
            
            
        
        <div class="col-md-6" style="border:1px solid #ccc; background:#ffdbab; min-height:400px;  padding:10px;">
            
            <h2> Reserve this Villa</h2>
            
            <?php echo $message; ?>
            
            <form method="post" action="confirm_booking.php">
               <div class="form-group">
                  <label> Check-in </label>
                   <input type="text" id="check-in" class="form-control" name="check-in" />
                </div>
                <div class="form-group">
                  <label> Check-out </label>
                   <input type="text" id="check-out" class="form-control" name="check-out" />
                </div>
                
                <div class="form-group">
                   <label> No of Adults </label>
                    <select class="form-control" id="ddl_no_of_adults" name="ddl_no_of_adults">
                       <option value="1"> 1 </option>
                        <option value="2"> 2 </option>
                         <option value="3"> 3 </option>
                         <option value="4"> 4 </option>
                         <option value="5"> 5 </option>
                         <option value="6"> 6 </option>
                         <option value="7"> 7 </option>
                         <option value="8"> 8 </option>
                    </select>
                </div>
                 <div class="form-group">
                   <label> No of children </label>
                    <select class="form-control" id="ddl_no_of_children" name="ddl_no_of_children">
                       <option value="0"> 0</option>
                        <option value="1"> 1</option>
                         <option value="2"> 2 </option>
                         <option value="3"> 3 </option>
                         <option value="4"> 4 </option>
                         <option value="5"> 5 </option>
                         <option value="6"> 6 </option>
                         <option value="7"> 7 </option>
                    </select>
                </div>
                <div id="amount_of_days"> </div>
                
                <input type="hidden" name="checkin" id="hidden_checkin"/>
                <input type="hidden" name="checkout" id="hidden_checkout"/>
                <input type="hidden" name="villa_id" value="<?php echo $id; ?>"/>
                <input type="hidden" name="customer_id"  value="<?php echo $customer_id;?>">
                <input type="hidden" name="no_of_days" id="no_of_days"  value=""/>
                <input  type="hidden" name="total_cost" id="total_cost" value=""/>
                <input type="hidden" name="total_guest" id="total_guest" value=""/>
                <input type="submit" class="btn btn-primary" id="check_availability" value="Make a reservation" />
            </form>
            
        </div>
 </div>
 