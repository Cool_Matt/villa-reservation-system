<?php 
session_start();
include("Config.php");
//includes the database connection string
include 'header.php'; 
//includes the page that holds the location to my stylesheet and other bootstrap/jquery connections
$sql = "SELECT * FROM villas";
//select statement to get all the villa data from database

$result = mysqli_query($db,$sql);
//Stores the result of the query under the variable $result

$villa_result= "";
?>

  <div id="banner">             
  </div>

<?php include 'nav.php'; ?>

  <div id="content_area">
     <?php
           if (mysqli_num_rows($result) > 0) //counts how many rows of data are found in the result of the query
           {
                while($row = mysqli_fetch_assoc($result) )
                    {
                         $villa_name =  $row['name'];
                         $address_line1 =  $row['address_line1'];
                         $address_line2 = $row['address_line2'];
                       $postcode = $row['postcode'];
                       $city =  $row['city'];
                       $daily_cost = $row['daily_cost'];
                       $img_link = $row['img_link'];
                        $id = $row['id'];
                        $short_description = $row['short_description'];
                        echo "<div class='col-md-12'>";
                        echo "<div class='col-md-4'> <img height='150' width='250' src='". $img_link . "'/></div>";
                        echo "<div class='col-md-8'>";
                        echo "<h2>". $villa_name. "</h2>";
                        echo "<p>";
                        echo "<span> <strong> Address: </strong> ". $address_line1. " , </span>";
                        echo "<span>". $address_line2. " , </span>";
                        echo "<span>". $postcode. ", </span>";
                        echo "<span>". $city . "</span>";
                        echo "</p>";
                         echo "<p>" . $short_description. "</p>";
                        echo "<p> £". $daily_cost . "<strong> per night </strong></p>";
                        echo "</div>";
                        echo "<div style='float:right;'><a class='btn btn-primary' href='view_villa.php?id=". $id . "'> View details </a></div>";
                      echo "</div>";
                    }
        }
        
        ?> 
 </div>
            


<?php include 'footer.php'; ?>