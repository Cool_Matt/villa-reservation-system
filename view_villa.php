<?php 
session_start();
include("Config.php");
//includes the database connection string
include 'header.php';
//includes the page that holds the location to my stylesheet and other bootstrap/jquery connections
$message = "";

if(isset($_GET['id'])) //Checks to see if ID has a value
{
    $id = $_GET['id']; //Sets the ID value to the variable $id
    
    $sql = "select * from villas where id = '$id'"; //Select statement gets all the villa  info where the villaid = the id we just got
    $result = mysqli_query($db, $sql);
    
    while($row = mysqli_fetch_array($result))
    {
        $return_id = $row['id'];
        $return_villa_name = $row['name'];
        $return_address_line1 = $row['address_line1'];
        $return_address_line2 = $row['address_line2'];
        $return_postcode = $row['postcode'];
        $return_city = $row['city'];
        $return_daily_cost = $row['daily_cost'];
        $return_short_description = $row['short_description'];
        $return_main_description = $row['main_description'];
        $return_img_link = $row['img_link'];
        //Gave each column in the row its own variable.
    }
}
 if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form
 //print_r($_POST);     
      $villa_id = mysqli_real_escape_string($db,$_POST['villa_id']);
      $checkin = mysqli_real_escape_string($db,$_POST['checkin']); 
      $checkout = mysqli_real_escape_string($db,$_POST['checkout']);  
     //mysqli_real_escape_string is a security method which escapes special characters in a string to then be used in an SQL statement,
      
      $check_villa_sql = "SELECT * FROM reservations where villa_id= '$villa_id' and ((check_in between '$checkin' and '$checkout') or (check_out between '$checkin' and '$checkout'))";
     //Select statement gets all the reservations within the selectedd dates of the user to make sure there are no clashes.
      
     //die($check_villa_sql);
     
      $result = mysqli_query($db,$check_villa_sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
       
      $count = mysqli_num_rows($result);
      
    // var_dump($row);
     
      // If result matched $myusername and $mypassword, table row must be 1 row
		
      if($count == 0 ) {
  
           if(isset($_SESSION['login_user']))
          {
              $message = "<div class='alert alert-warning alert-dismissible' role='alert'>
                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                  <strong>This Villa is available for the dates you  searched for. <a href='book_villa.php?id=".$villa_id."'>Reserve this Villa </a></strong>
                </div>";
          }else{
              $message = "<div class='alert alert-warning alert-dismissible' role='alert'>
                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                  <strong>This Villa is available for the dates you searched for but you need to login or sign up to reserve this Villla</strong>
                </div>";
              ;
          }
      }
      else 
      {
          $message = "<div class='alert alert-warning alert-dismissible' role='alert'>
                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                  <strong>This Villa has already been booked within the dates you searched for.</strong>
                </div>";
               
      }
   }



?>
<script>
 $( function() {
     
     
    $( "#check-in" ).datepicker({  //initialises the UI control 
        dateFormat: 'yy-mm-dd' ,   //sets in what way the dateformat will be saved as
        minDate: 0                 //ensures thats you cant choose days in the past
    }).val();                      //gets the value from the function
     
     
     
    $( "#check-out" ).datepicker({ 
        dateFormat: 'yy-mm-dd' ,
        minDate:0   
    }).val();
     
    $("#check_availability").click(function() {
      //alerting the value inside the textbox
      var rawdate1 = $("#check-in").datepicker("getDate");     //gets the checkin date under variable rawdate1
      var check_in_date = $.datepicker.formatDate("yy-mm-dd", rawdate1);   //reformats the date you just chose in the format we need under a different variable
       
      var rawdate2 = $("#check-out").datepicker("getDate");     //gets the checkout date under variable rawdate2
      var check_out_date = $.datepicker.formatDate("yy-mm-dd", rawdate2);   //reformats the date you just chose in the format we need under a different variable
     
        $("#hidden_checkin").val(check_in_date);       //Sets the hidden input as the reformated date to be used on other pages
        $("#hidden_checkout").val(check_out_date);;
        
        var diff =  Math.floor(( Date.parse(check_out_date) - Date.parse(check_in_date) ) / 86400000);     //86400000 is number of milliseconds in the day so we know what day it was booked on
        
        var dayText = ""
        if(diff == 1){
            dayText ="day";
        }else{
            dayText= "days"
        }
        
      $("#amount_of_days").append(diff + " " + dayText);
        
    
    });
     
  } );
</script>
 
  <?php echo $message; ?>
<?php include 'nav.php'; ?>
 

  <div class="villa_banner" style="background-image:url('<?php echo $return_img_link;?>')">             
  </div>
 
 
  <div id="container">
    
    <div class="col-md-12" style="margin-top:10px; margin-bottom:10px;">
        <div class="col-md-8">
            <div class="panel panel-primary">
      <div class="panel-heading"><h1> <?php echo $return_villa_name ; ?> </h1></div>
      <div class="panel-body"><?php echo $return_address_line1 . ", ". $return_address_line2 . ", " . $return_postcode; ?>
        <p>
          <?php echo $return_short_description ;?>
        </p>
        <p>
            <?php echo $return_main_description ; ?>
        </p>
        </div></div>
    </div> 
            
            
        
        <div class="col-md-4" style="border:1px solid #ccc; background:#ffdbab; min-height:400px;  padding:10px;">
           <?php echo $message; ?>
            <form method="post" action="view_villa.php?id=<?php echo $id;?>">
               <div class="form-group">
                  <label> Check-in </label>
                   <input type="text" id="check-in" class="form-control" name="check-in" />
                </div>
                <div class="form-group">
                  <label> Check-out </label>
                   <input type="text" id="check-out" class="form-control" name="check-out" />
                </div>
                
                <div class="form-group">
                   <label> No of Adults </label>
                    <select class="form-control" id="ddl_no_of_adults" name="ddl_no_of_adults">
                       <option value="1"> 1 </option>
                        <option value="2"> 2 </option>
                         <option value="3"> 3 </option>
                         <option value="4"> 4 </option>
                         <option value="5"> 5 </option>
                         <option value="6"> 6 </option>
                         <option value="7"> 7 </option>
                         <option value="8"> 8 </option>
                    </select>
                </div>
                 <div class="form-group">
                   <label> No of children </label>
                    <select class="form-control" id="ddl_no_of_children" name="ddl_no_of_children">
                       <option value="0"> 0</option>
                        <option value="1"> 1</option>
                         <option value="2"> 2 </option>
                         <option value="3"> 3 </option>
                         <option value="4"> 4 </option>
                         <option value="5"> 5 </option>
                         <option value="6"> 6 </option>
                         <option value="7"> 7 </option>
                    </select>
                </div>
                <div id="amount_of_days"> </div>
                <input type="hidden" name="checkin" id="hidden_checkin"/>
                <input type="hidden" name="checkout" id="hidden_checkout"/>
                <input type="hidden" name="villa_id" value="<?php echo $id; ?>"/>
                <input type="submit" class="btn btn-primary" id="check_availability" value="Check availability" />
            </form>
            
        </div>
 </div>
           
 

<?php include 'footer.php'; ?>