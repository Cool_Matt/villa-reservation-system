<?php 
session_start();
include("../Config.php");

if(!isset($_SESSION["admin_user"])){
	header("location: index.php?msg=no backdoor!");
    exit();
}


include '../header.php'; 


$sql = "SELECT reservations.check_in, reservations.customer_id,reservations.check_out,reservations.total_cost, reservations.total_guests, reservations.date_of_reservation, villas.name AS Villa_name,                    customers.first_name,customers.last_name
              FROM reservations 
              LEFT JOIN villas ON reservations.villa_id = villas.id
              LEFT JOIN customers ON  reservations.customer_id = customers.id";

$result = mysqli_query($db,$sql);


?>

  <div id="banner">             
  </div>


<?php include 'nav.php';?>


  <div id="content_area">
      
     
      <h2> All Bookings </h2>
   
          <table class="table table-bordered">
         
              <tr>
                <th> Customer Name </th>
                <th>Villa Name</th>
                <th>Reservation date</th>
                <th> Check-in </th>
                <th> Check-out  </th>
                <th> Total cost </th>
                <th> Total Number of Guests </th>
              </tr>
              <?php
           if (mysqli_num_rows($result) > 0) 
           {
                while($row = mysqli_fetch_assoc($result) )
                    {
                         $first_name = $row['first_name'];
                         $last_name = $row['last_name'];
                         $villa_name =  $row['Villa_name'];
                         $date =  $row['date_of_reservation'];
                         $check_in =  $row['check_in'];
                         $check_out  = $row['check_out'];
                         $total_cost = $row['total_cost'];
                        $total_guests =  $row['total_guests'];
                 

                        echo "<tr>";
                        echo "<td>". $first_name. " ". $last_name ."</td>";
                        echo "<td>". $villa_name. "</td>";
                        echo "<td>". $date. "</td>";
                        echo "<td>". $check_in. "</td>";
                        echo "<td>". $check_out. "</td>";
                        echo "<td>". $total_cost . "</td>";
                        echo "<td>". $total_guests ."</td>";
                       
                      echo "</tr>";
                    }
        }
        
        ?>
          
        </table>  
 </div>
            


<?php include '../footer.php'; ?>