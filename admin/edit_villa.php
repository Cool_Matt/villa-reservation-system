<?php 
session_start();
include("../Config.php");

if(!isset($_SESSION["admin_user"])){
	header("location: index.php?msg=no backdoor!");
    exit();
}


   $message = "";
 if($_SERVER["REQUEST_METHOD"] == "POST") 
 {
     $villa_id = mysqli_real_escape_string($db,$_POST['thisID']);
     
     
     if(!empty($_POST['villa_name'])){
         $villa_name = mysqli_real_escape_string($db,$_POST['villa_name']);  
     }
      
    if(!empty($_POST['address_line1'])){
        $address_line1 = mysqli_real_escape_string($db,$_POST['address_line1']);  
    }
     
  if(!empty($_POST['address_line2'])){
        $address_line2 = mysqli_real_escape_string($db,$_POST['address_line2']);  
    }
     
     if(!empty($_POST['postcode'])){
        $postcode = mysqli_real_escape_string($db,$_POST['postcode']);  
    }
     
    if(!empty($_POST['city'])){
        $city = mysqli_real_escape_string($db,$_POST['city']);  
    }
     
    if(!empty($_POST['daily_cost'])){
        $daily_cost = mysqli_real_escape_string($db,$_POST['daily_cost']);  
    }
     
      if(!empty($_POST['short_description'])){
        $short_description = mysqli_real_escape_string($db,$_POST['short_description']);  
    }
    
    if(!empty($_POST['main_description'])){
        $main_description = mysqli_real_escape_string($db,$_POST['main_description']);  
    }
     
     
    if ($_FILES['villa_image']['tmp_name'] != "") 
    {
        $fileName = $_FILES["villa_image"]["name"]; // The file name
            $fileTmpLoc = $_FILES["villa_image"]["tmp_name"]; //The file tmp name

            $uploads_dir = 'uploads/';

            if (!$fileTmpLoc) 
            { 
                echo "ERROR: Please upload an image for the Villa.";
                exit();
            }
         
         $file_upload_result = move_uploaded_file($fileTmpLoc, "../uploads/$fileName" );
         if ($file_upload_result = true) 
         {
            $villa_img_link = $uploads_dir.$fileName;
         }
         else
         { 
            echo "ERROR: File not uploaded. Try again.";
            unlink($fileTmpLoc); // Remove the uploaded file from the PHP temp folder
            exit();
         }
        
		$sql = "UPDATE villas SET name='$villa_name', address_line1='$address_line1', address_line2='$address_line2', postcode='$postcode', city='$city', img_link='$villa_img_link', short_description='$short_description',main_description='$main_description', daily_cost='$daily_cost' WHERE id='$villa_id'";
  
        
         $query = mysqli_query($db, $sql); 
         if($query){
           header("location:villas.php");
         }else{
             $message = "Could not add villa. Please try again";
         }
  }
     else
    {
        	$sql = "UPDATE villas SET name='$villa_name', address_line1='$address_line1', address_line2='$address_line2', postcode='$postcode', city='$city', short_description='$short_description',main_description='$main_description', daily_cost='$daily_cost' WHERE id='$villa_id'";
  //die($sql);
         $query = mysqli_query($db, $sql); 
         if($query){
           header("location:villas.php");
         }else{
             $message = "Could not add villa. Please try again";
         }
    } 
     
     
     //
     
 }

if(isset($_GET['id']))
{
    $targetID = $_GET['id'];
    $sql = "SELECT * FROM villas where id = '$targetID'";
    $result  = mysqli_query($db, $sql);
    $return_img_link ='';
    $villa_img_link = '';
    while($row = mysqli_fetch_array($result))
    {
        $return_id = $row['id'];
        $return_villa_name = $row['name'];
        $return_address_line1 = $row['address_line1'];
        $return_address_line2 = $row['address_line2'];
        $return_postcode = $row['postcode'];
        $return_city = $row['city'];
        $return_daily_cost = $row['daily_cost'];
        $return_short_description = $row['short_description'];
        $return_main_description = $row['main_description'];
        $return_img_link = $row['img_link'];
    }
    
    
 
    
}
 


include '../header.php'; 

?>

  <div id="banner">             
  </div>


<?php include 'nav.php';?>



  <div id="container">
      
      <div class="row">
        <div class="col-md-8" style="margin:auto auto auto auto; float:none;">
             <a class="btn btn-success" href="villas.php"> Back to Villas </a>
      <h2> View | Edit Villa</h2>

            <?php echo $message; ?>
            
            <img src="../<?php echo $return_img_link;?>" height='350' width='750'/>
   <form method="post" action="edit_villa.php" enctype="multipart/form-data">
      <div class="form-group">
         <label> Name </label>
          <input type="text" class="form-control" name="villa_name" value="<?php echo $return_villa_name; ?>" required /> 
       </div>
       
        <div class="form-group">
         <label> Address Line 1 </label>
          <input type="text" class="form-control" name="address_line1" value="<?php echo $return_address_line1; ?> " required /> 
       </div>
       
        <div class="form-group">
         <label> Address Line 2 </label>
          <input type="text" class="form-control" name="address_line2" value="<?php echo $return_address_line2; ?>"  /> 
       </div>
       
        <div class="form-group">
         <label> Postcode </label>
          <input type="text" class="form-control" name="postcode" value="<?php echo $return_postcode ; ?>" required /> 
       </div>
       
        <div class="form-group">
         <label> City </label>
          <input type="text" class="form-control" name="city" value="<?php echo $return_city;?>  " required /> 
       </div>
       
        <div class="form-group">
         <label> Daily Cost(GBP) </label>
          <input type="text" class="form-control" name="daily_cost" value="<?php echo $return_daily_cost; ?>" required /> 
       </div>
       
     <div class="form-group">
         <label> Short description </label>
         <textarea class="form-control" name="short_description" rows="5" required>
           <?php echo $return_short_description; ?>
         </textarea>
       </div>
       
       <div class="form-group">
         <label> Main description </label>
         <textarea class="form-control" name="main_description" rows="8" required>
           <?php echo $return_main_description;?>
         </textarea>
       </div>
       
        <div class="form-group">
         <label> Villa image </label>
          <input type="file" class="form-control" name="villa_image"  /> 
       </div>
         <input name="thisID" type="hidden" value="<?php echo $targetID; ?>" />
       <input type="submit" value="Upload Villa Details" class="btn btn-success">
   </form>
      
          
        </div>
      
      </div>
      
      
 </div>
            
 <div id="sidebar">
                 
 </div>

<?php include '../footer.php'; ?>