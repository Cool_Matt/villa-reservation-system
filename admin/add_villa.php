<?php 
session_start();
include("../Config.php");

if(!isset($_SESSION["admin_user"])){
	header("location: index.php?msg=no backdoor!");
    exit();
}

$message = "";
 if($_SERVER["REQUEST_METHOD"] == "POST") 
 {
     if(!empty($_POST['villa_name'])){
         $villa_name = mysqli_real_escape_string($db,$_POST['villa_name']);  
     }
      
    if(!empty($_POST['address_line1'])){
        $address_line1 = mysqli_real_escape_string($db,$_POST['address_line1']);  
    }
     
  if(!empty($_POST['address_line2'])){
        $address_line2 = mysqli_real_escape_string($db,$_POST['address_line2']);  
    }
     
     if(!empty($_POST['postcode'])){
        $postcode = mysqli_real_escape_string($db,$_POST['postcode']);  
    }
     
    if(!empty($_POST['city'])){
        $city = mysqli_real_escape_string($db,$_POST['city']);  
    }
     
    if(!empty($_POST['daily_cost'])){
        $daily_cost = mysqli_real_escape_string($db,$_POST['daily_cost']);  
    }
     
      if(!empty($_POST['short_description'])){
        $short_description = mysqli_real_escape_string($db,$_POST['short_description']);  
    }
    
    if(!empty($_POST['main_description'])){
        $main_description = mysqli_real_escape_string($db,$_POST['main_description']);  
    }
     
     
    $fileName = $_FILES["villa_image"]["name"]; // The file name
    $fileTmpLoc = $_FILES["villa_image"]["tmp_name"]; //The file tmp name
     
    $uploads_dir = 'uploads/';
     
    if (!$fileTmpLoc) 
    { 
        echo "ERROR: Please upload an image for the Villa.";
        exit();
    }
     
     if(isset($_FILES['villa_image']))
     {
         $file_upload_result = move_uploaded_file($fileTmpLoc, "../uploads/$fileName" );
         if ($file_upload_result = true) 
         {
            $villa_img_link = $uploads_dir.$fileName;
         }
         else
         { 
            echo "ERROR: File not uploaded. Try again.";
            unlink($fileTmpLoc); // Remove the uploaded file from the PHP temp folder
            exit();
         }
     }
         
     // Add user info into the database table for the main site table
		$sql = "INSERT INTO villas (name, address_line1, address_line2, postcode, city, img_link, short_description,main_description, daily_cost)       
		        VALUES('$villa_name','$address_line1','$address_line2','$postcode','$city', '$villa_img_link','$short_description','$main_description','$daily_cost')";
     
     $query = mysqli_query($db, $sql); 
     if($query){
         $message = "Villa successfully added";
     }else{
         $message = "Could not add villa. Please try again";
     }
     
 }

include '../header.php'; 

?>

  <div id="banner">             
  </div>


<?php include 'nav.php';?>


  <div id="container">
      
      <div class="row">
        <div class="col-md-8" style="margin:auto auto auto auto; float:none;">
             <a class="btn btn-success" href="villas.php"> Back to Villas </a>
      <h2> Add a new Villa</h2>

            <?php echo $message; ?>
   <form method="post" action="add_villa.php" enctype="multipart/form-data">
      <div class="form-group">
         <label> Name </label>
          <input type="text" class="form-control" name="villa_name" required /> 
       </div>
       
        <div class="form-group">
         <label> Address Line 1 </label>
          <input type="text" class="form-control" name="address_line1" required /> 
       </div>
       
        <div class="form-group">
         <label> Address Line 2 </label>
          <input type="text" class="form-control" name="address_line2"  /> 
       </div>
       
        <div class="form-group">
         <label> Postcode </label>
          <input type="text" class="form-control" name="postcode" required /> 
       </div>
       
        <div class="form-group">
         <label> City </label>
          <input type="text" class="form-control" name="city" required /> 
       </div>
       
        <div class="form-group">
         <label> Daily Cost(GBP) </label>
          <input type="text" class="form-control" name="daily_cost" required /> 
       </div>
       
     <div class="form-group">
         <label> Short description </label>
         <textarea class="form-control" name="short_description" rows="5" required>
         
         </textarea>
       </div>
       
       <div class="form-group">
         <label> Main description </label>
         <textarea class="form-control" name="main_description" rows="8" required>
         
         </textarea>
       </div>
       
        <div class="form-group">
         <label> Villa image </label>
          <input type="file" class="form-control" name="villa_image" required /> 
       </div>
       
       <input type="submit" value="Upload Villa Details" class="btn btn-success">
   </form>
      
          
        </div>
      
      </div>
      
      
 </div>
            
 <div id="sidebar">
                 
 </div>

<?php include '../footer.php'; ?>