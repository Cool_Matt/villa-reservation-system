<?php 
session_start();
include("../Config.php");

if(!isset($_SESSION["admin_user"])){
     header("location: index.php?msg=no backdoor!");
    exit();
}

include '../header.php'; 


$month = "";

 if($_SERVER["REQUEST_METHOD"] == "POST") 
 {
     if(!empty($_POST['month'])){
          $month = mysqli_real_escape_string($db,$_POST['month']);
     }
      
      if(!empty($_POST['year'])){
          $year = mysqli_real_escape_string($db,$_POST['year']);
     }
     
     $bill_sql = "SELECT * FROM bills WHERE MONTH(date) = '$month'";
     $query_result = mysqli_query($db, $bill_sql) or mysqli_error($db);
     
     $total_sql =  "SELECT SUM(amount) AS total FROM bills WHERE MONTH(date) = '$month'";
     $total_result = mysqli_query($db, $total_sql) or mysqli_error($db);
     
 }
 else
 {
     $bill_sql = "SELECT * FROM bills WHERE MONTH(date) = '01'";
     $query_result = mysqli_query($db, $bill_sql) or mysqli_error($db); 
     
      $total_sql =  "SELECT SUM(amount) AS total FROM bills WHERE MONTH(date) = '01'";
     $total_result = mysqli_query($db, $total_sql) or mysqli_error($db);
 }

?>


  <div id="banner">             
  </div>


<?php include 'nav.php';?>


  <div id="content_area">
      
       <a class="btn btn-success" href="add_bills.php"> Add a new expense </a>
          <a class="btn btn-success" href="expenses.php"> Go back to expenses </a>
      <h2> All Expenses </h2>
   
      <div class="row">
        <div class="col-md-8">
               <form method="post" action="filter_expenses.php">
                 <h4> Filter expenses</h4>
                  <div class="form-inline">
                     <div class="form-group col-md-4">
                         <label> Month </label>
                         <select name="month" id="month" class="form-control"> 
                            <option value="">--Please select a month to filter by-- </option>
                            <option value="01">  January  </option> 
                            <option value="02">  February  </option>  
                            <option value="03">  March  </option>  
                            <option value="04">  April  </option>  
                            <option value="05">  May  </option>   
                            <option value="06">  June  </option> 
                            <option value="07">  July  </option> 
                            <option value="08">  August  </option>  
                            <option value="09">  September  </option>   
                            <option value="10">  October  </option>  
                            <option value="11">  November  </option>   
                            <option value="12">  December  </option>  
                         </select>
                      </div>   
                      <div class="form-group col-md-4">
                          <label> Year </label>
                         <select name="year" id="year" class="form-control"> 
                            <option value="2017">2017 </option>
                            <option value="2018">  2018  </option> 
                            <option value="2019">  2019  </option>  
                         </select>
                      </div> 
                      <input type="submit" name="search" id="search" class="btn btn-success" value="Search"/>
                  </div>
            </form>
        </div>
      
      </div>
     
      <div id="table_filtered">
              <table class="table table-bordered" id="">
            <h1> Expenses by Month/Year </h1>
              <tr>
                <th> Expense </th>
                <th>  Amount </th>
                <th> Date </th>
                <th> Notes </th>
            
              </tr>
              <?php
           if (mysqli_num_rows($query_result) > 0) 
           {
                while($row = mysqli_fetch_assoc($query_result) )
                    {
                         $query_result_expense =  $row['expense'];
                         $query_result_amount =  $row['amount'];
                         $query_result_date = $row['date'];
                         $query_result_notes = $row['notes'];
                           

                        echo "<tr >";
                        echo "<td>". $query_result_expense . "</td>";
                        echo "<td>". $query_result_amount. "</td>";
                        echo "<td>". $query_result_date. "</td>";
                        echo "<td>". $query_result_notes. "</td>";
                      echo "</tr>";
                    }
               
                $total_row = mysqli_fetch_assoc($total_result);
               
                        echo "<tr>";
                        echo "<td></td>";
                        echo "<td> Total <strong>".  round($total_row['total'],2) . "</strong></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "</tr>";
            
        }
        
        ?>
          
        </table> 
      </div>
         
  
  
        
      
 </div>
            
 <div id="sidebar">
                 
 </div>

<?php include '../footer.php'; ?>