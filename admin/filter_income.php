<?php 
session_start();
include("../Config.php");

if(!isset($_SESSION["admin_user"])){
     header("location: index.php?msg=no backdoor!");
    exit();
}

include '../header.php'; 


$month = "";

 if($_SERVER["REQUEST_METHOD"] == "POST") 
 {
     if(!empty($_POST['month'])){
          $month = mysqli_real_escape_string($db,$_POST['month']);
     }
      
      if(!empty($_POST['year'])){
          $year = mysqli_real_escape_string($db,$_POST['year']);
     }
     
     $income_sql = "SELECT SUM(total_cost) AS total,MONTH(check_in) AS month, YEAR(check_in) AS year FROM reservations WHERE MONTH(check_in) = '$month' GROUP BY month,year ";
     $query_result = mysqli_query($db, $income_sql) or mysqli_error($db);
     
 }
 else
 {
     $income_sql = "SELECT SUM(total_cost) AS total,MONTH(check_in) AS month, YEAR(check_in) AS year FROM reservations WHERE MONTH(check_in) = '01' GROUP BY month,year ";
     $query_result = mysqli_query($db, $income_sql) or mysqli_error($db); 
     
 }

?>


  <div id="banner">             
  </div>


<?php include 'nav.php';?>


  <div id="content_area">
      
      
      <a class="btn btn-success" href="income.php"> Go back to all Income </a>
     
   
      <div class="row">
        <div class="col-md-8">
               <form method="post" action="filter_income.php">
                 <h4> Filter expenses</h4>
                  <div class="form-inline">
                     <div class="form-group col-md-4">
                         <label> Month </label>
                         <select name="month" id="month" class="form-control"> 
                            <option value="">--Please select a month to filter by-- </option>
                            <option value="01">  January  </option> 
                            <option value="02">  February  </option>  
                            <option value="03">  March  </option>  
                            <option value="04">  April  </option>  
                            <option value="05">  May  </option>   
                            <option value="06">  June  </option> 
                            <option value="07">  July  </option> 
                            <option value="08">  August  </option>  
                            <option value="09">  September  </option>   
                            <option value="10">  October  </option>  
                            <option value="11">  November  </option>   
                            <option value="12">  December  </option>  
                         </select>
                      </div>   
                      <div class="form-group col-md-4">
                          <label> Year </label>
                         <select name="year" id="year" class="form-control"> 
                            <option value="2017">2017 </option>
                            <option value="2018">  2018  </option> 
                            <option value="2019">  2019  </option>  
                         </select>
                      </div> 
                      <input type="submit" name="search" id="search" class="btn btn-success" value="Search"/>
                  </div>
            </form>
        </div>
      
      </div>
     
      <div id="table_filtered">
             <table class="table table-bordered" id="">
              <tr>
                <th> Total income in GBP </th>
                <th>  Month </th>
                <th> Year </th>
              </tr>
              <?php
           if (mysqli_num_rows($query_result) > 0) 
           {
                while($row = mysqli_fetch_assoc($query_result) )
                    {
                         $total =  $row['total'];
                         $month = $row['month'];
                         $year = $row['year'];
                        
                        echo "<tr >";
                        echo "<td>". $total . "</td>";
                        echo "<td>". $month ."</td>";
                        echo "<td>". $year  ."</td>";
                        
                      echo "</tr>";
                    }
        }
        
        ?>
          
        </table> 
      </div>
         
  
  
        
      
 </div>
            
 <div id="sidebar">
                 
 </div>

<?php include '../footer.php'; ?>