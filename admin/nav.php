 <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Serenity Star</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="admin_dashboard.php">Dashboard</a></li>
      <li><a href="villas.php"> Villas </a></li>
        <li><a href="all_bookings.php"> All bookings </a></li>
      <li><a href="expenses.php">Expenses</a></li>
      <li><a href="income.php">Income</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>