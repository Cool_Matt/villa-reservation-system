<?php 
session_start();
include("../Config.php");

if(!isset($_SESSION["admin_user"])){
	header("location: index.php?msg=no backdoor!");
    exit();
}

$message = "";

 if($_SERVER["REQUEST_METHOD"] == "POST") 
 {
    
     
     if(!empty($_POST['expense'])){
          $expense = mysqli_real_escape_string($db,$_POST['expense']);
     }
      
 
     $bill_date = $_POST['hidden_date'];  
    
     
  if(!empty($_POST['amount'])){
        $amount = mysqli_real_escape_string($db,$_POST['amount']);  
    }
    
    if(!empty($_POST['notes'])){
        $notes = mysqli_real_escape_string($db,$_POST['notes']);  
    }  
     
  
     // Add user info into the database table for the main site table
		
     $sql = "INSERT INTO bills (expense, amount, date, notes)       
		        VALUES('$expense','$amount','$bill_date','$notes')";
     
     $query = mysqli_query($db, $sql) or mysqli_error($db); 
     if($query){
         $message = "Bill successfully added";
     }else{
         $message = "Could not add Bill. Please try again";
     }
     
 }

include '../header.php'; 

?>

  <div id="banner">             
  </div>


<?php include 'nav.php';?>

<script>
 $(function() {
     
     
    $("#bill_date").datepicker({ 
        dateFormat: 'yy-mm-dd' 
      
    }).val();
     
     
    $("#upload_bill").click(function() {
      //alerting the value inside the textbox
      var raw_bill_date = $("#bill_date").datepicker("getDate");
      var bill_date1 = $.datepicker.formatDate("yy-mm-dd", raw_bill_date);
        
      console.log(bill_date1);
      
     $("#hidden_date").val(bill_date1);
        
    });
     
  } );
</script>

  <div id="container">
      
      <div class="row">
        <div class="col-md-8" style="margin:auto auto auto auto; float:none;">
             <a class="btn btn-success" href="index.php"> Back to Home</a>
      <h2> View | Edit Bills</h2>

            <?php echo $message; ?>
            
            
   <form method="post" action="add_bills.php">
      <div class="form-group">
         <label> Expense </label>
          <input type="text" class="form-control" name="expense" id="expense"value="" required /> 
       </div>
       
        <div class="form-group">
         <label> Date: </label>
          <input type="text" class="form-control" id="bill_date" name="bill_date" value="" required /> 
       </div>
       
        <div class="form-group">
         <label> Amount  : </label>
          <input type="text" min="1"  class="form-control" name="amount" id="amount" value="" required /> 
       </div>
       
        <div class="form-group">
         <label> Notes : </label>
          <textarea name="notes" id="notes" rows="3" class="form-control" required>
            
          </textarea>
       </div>
       
      
         <input name="hidden_date" id="hidden_date" type="hidden" value="" />
       <input type="submit" value="Upload Bill Details" name="upload_bill" id="upload_bill" class="btn btn-success">
   </form>
      
          
        </div>
      
      </div>
      
      
 </div>
            
 <div id="sidebar">
                 
 </div>

<?php include '../footer.php'; ?>