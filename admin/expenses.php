<?php 
session_start();
include("../Config.php");

if(!isset($_SESSION["admin_user"])){
     header("location: index.php?msg=no backdoor!");
    exit();
}

include '../header.php'; 

$sql = "SELECT * FROM bills";

$result = mysqli_query($db,$sql);


?>


  <div id="banner">             
  </div>


<?php include 'nav.php';?>


  <div id="content_area">
      
       <a class="btn btn-success" href="add_bills.php"> Add a new expense </a>
      <a class="btn btn-success" href="filter_expenses.php"> Search for expenses by month </a>
      <h2> All Expenses </h2>

  
      <div id="table_original">
             <table class="table table-bordered" id="">
              <tr>
                <th> Expense </th>
                <th>  Amount </th>
                <th> Date </th>
                <th> Notes </th>
              </tr>
              <?php
           if (mysqli_num_rows($result) > 0) 
           {
                while($row = mysqli_fetch_assoc($result) )
                    {
                         $expense =  $row['expense'];
                         $amount =  $row['amount'];
                         $date = $row['date'];
                         $notes = $row['notes'];
                      

                        echo "<tr >";
                        echo "<td>". $expense . "</td>";
                        echo "<td>". $amount. "</td>";
                        echo "<td>". $date. "</td>";
                        echo "<td>". $notes. "</td>";
                      echo "</tr>";
                    }
        }
        
        ?>
          
        </table>  
      </div>
        
      
 </div>
            
 <div id="sidebar">
                 
 </div>

<?php include '../footer.php'; ?>