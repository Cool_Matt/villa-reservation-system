<?php 
session_start();
include("../Config.php");

if(!isset($_SESSION["admin_user"])){
     header("location: index.php?msg=no backdoor!");
    exit();
}

include '../header.php'; 

$sql = "SELECT * FROM villas";

$result = mysqli_query($db,$sql);

$villa_result= "";



?>

  <div id="banner">             
  </div>


<?php include 'nav.php';?>


  <div id="content_area">
      
         <a class="btn btn-success" href="add_villa.php"> Add a new villa </a>
      <h2> All Villas</h2>
   
          <table class="table table-bordered">
         
              <tr>
                <th>Name</th>
                <th>AddressLine 1</th>
                <th> Address Line 2</th>
                <th> Postcode </th>
                <th> City </th>
                <th> Daily cost </th>
                <th> Photo </th>
                  <th> Action </th>
              </tr>
              <?php
           if (mysqli_num_rows($result) > 0) 
           {
                while($row = mysqli_fetch_assoc($result) )
                    {
                         $villa_name =  $row['name'];
                         $address_line1 =  $row['address_line1'];
                         $address_line2 = $row['address_line2'];
                       $postcode = $row['postcode'];
                       $city =  $row['city'];
                       $daily_cost = $row['daily_cost'];
                       $img_link = $row['img_link'];
                        $id = $row['id'];

                        echo "<tr>";
                        echo "<td>". $villa_name. "</td>";
                        echo "<td>". $address_line1. "</td>";
                        echo "<td>". $address_line2. "</td>";
                        echo "<td>". $postcode. "</td>";
                        echo "<td>". $city . "</td>";
                        echo "<td>". $daily_cost . "</td>";
                        echo "<td> <img height='150' width='250' src='../". $img_link . "'/></td>";
                        echo "<td><a href='edit_villa.php?id=". $id . "'> View details </a></td>";
                      echo "</tr>";
                    }
        }
        
        ?>
          
        </table>  
 </div>
            
 <div id="sidebar">
                 
 </div>

<?php include '../footer.php'; ?>