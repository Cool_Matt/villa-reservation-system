<?php

session_start();
include("Config.php");

$message = "";
 if($_SERVER["REQUEST_METHOD"] == "POST") 
 {
     if(!empty($_POST['first_name'])){
         $first_name = mysqli_real_escape_string($db,$_POST['first_name']);  
     }
      
    if(!empty($_POST['last_name'])){
        $last_name = mysqli_real_escape_string($db,$_POST['last_name']);  
    }
     
  if(!empty($_POST['email'])){
        $email = mysqli_real_escape_string($db,$_POST['email']);  
    }
     
     if(!empty($_POST['password'])){
        $password = mysqli_real_escape_string($db,$_POST['password']);  
    }
    
    if(!empty($_POST['confirm_password'])){
        $confirm_password = mysqli_real_escape_string($db,$_POST['confirm_password']);  
    }
    
     
    if(!empty($_POST['mobile'])){
        $mobile = mysqli_real_escape_string($db,$_POST['mobile']);  
    }
     
    if(!empty($_POST['address_line_1'])){
        $address_line_1 = mysqli_real_escape_string($db,$_POST['address_line_1']);  
    }
     
    if(!empty($_POST['city'])){
        $city = mysqli_real_escape_string($db,$_POST['city']);  
    }
     
    if(!empty($_POST['postcode'])){
        $postcode = mysqli_real_escape_string($db,$_POST['postcode']);  
    }
     
     $date_created = date("Y-m-d");
     
     if($password !== $confirm_password)
     {
        header("location:SignUp.php?message=Passwords Did Not Match");
     }else{
	 	$sql = "INSERT INTO customers (first_name, last_name, email, password, mobile, address_line_1, city, postcode, date_created)       
		        VALUES('$first_name','$last_name','$email','$password','$mobile','$address_line_1','$city','$postcode','$date_created')";
     
	 }
     
     // Add user info into the database table for the main site table
	/* 	$sql = "INSERT INTO customers (first_name, last_name, email, password, mobile, address_line_1, city, postcode, date_created)       
		        VALUES('$first_name','$last_name','$email','$password','$mobile','$address_line_1','$city','$postcode','$date_created')";
      */
      $query = mysqli_query($db, $sql); 
     if($query){
         $message = "<div class='alert alert-success alert-dismissible' role='alert'>
                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                  <strong>Account Creation Was A Success!</a></strong>
                </div>";
     }else{
         $message = "<div class='alert alert-warning alert-dismissible' role='alert'>
                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                  <strong>Account Creation Failed Please Try Again</a></strong>
                </div>";
     }
     
 }
     
?>

<?php include 'header.php'; ?>
  <div id="banner">             
  </div>
<?php include 'nav.php'; ?>

 <div class="container">
     <div class="row">
           <div class="col-md-12">
              <div style = "background-color:#333333; color:#FFFFFF; padding:3px;"><b>Sign Up</b></div>  
               
               <?php echo $message ;
                  if(!empty($_GET['message'])) 
                  {
                  $message = $_GET['message'];
                   echo $message;      
                  }
               ?>
      <form action="signup.php" method="POST">
          
            <legend>Personal information:</legend>
              <div class="form-group">
                  <label> First name</label> 
                  <input type="text" class="form-control" required name="first_name">
              </div>
          
            <div class="form-group">
               <label> Last name </label>
                <input type="text" name="last_name" required class="form-control">
                <br>
            </div>
            
              <div class="form-group">
                  <label> Email </label>
                 <input type="email" name="email" requiredrequired class="form-control" placeholder="">
              </div>
          
           <div class="form-group">
               <label> Password </label>
                <input type="password" name="password" required class="form-control">
                <br>
            </div>
            <div class="form-group">
               <label> Confirm Password </label>
                <input type="password" name="confirm_password" required class="form-control">
                <br>
            </div>
              <div class="form-group">
                   <label> Mobile  </label>
                 <input type="text" name="mobile" class="form-control" placeholder="">
              </div>
            
            <div class="form-group">
                <label> Address Line 1  </label>
                 <input type="text" name="address_line_1" required class="form-control" placeholder="">
            </div>
              
              <div class="form-group">
                 <label> City  </label>
                 <input type="text" name="city" required class="form-control" placeholder="">
              </div>
              
              <div class="form-group">
                 <label> Postcode   </label>
                 <input type="text" name="postcode" required class="form-control" placeholder="">
              </div>
              
            <input name="submit" type="submit" class="btn btn-success" value="SignUp">
          
        </form>
               
               <div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php /*echo $error;*/ ?></div>
      </div>
     </div>
</div>
     

<?php include 'footer.php'; ?>